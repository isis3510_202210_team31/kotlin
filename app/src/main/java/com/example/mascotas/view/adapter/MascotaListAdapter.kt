package com.example.mascotas.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mascotas.R
import com.example.mascotas.model.MascotaModel
import com.example.mascotas.view.ui.fragments.MascotaDetailDialogFragment

class MascotaListAdapter(val mascotasList: List<MascotaModel>, val fragmentManager: FragmentManager) : RecyclerView.Adapter<MascotaListAdapter.MascotaHolder>() {
    class MascotaHolder(val view: View, val fragmentManager: FragmentManager): RecyclerView.ViewHolder(view){
        fun render(mascota: MascotaModel){
            //TODO Si todo sale mal, toca con cardview
            var row = view.findViewById<LinearLayout>(R.id.lyMascota)
            var imgMascota = view.findViewById<ImageView>(R.id.imgMascota)
            var tvNombre = view.findViewById<TextView>(R.id.tvNombre)
            var tvEspecie = view.findViewById<TextView>(R.id.tvEspecie)

            if(mascota.imgPet == null){
                imgMascota.setImageResource(R.drawable.beluga_example)
            }else{
                imgMascota.setImageResource(R.drawable.beluga_example)
            }

            tvNombre.text = mascota.name
            tvEspecie.text = mascota.species

            row.setOnClickListener{ view: View ->
                /**
                 * id: Long
                 * name: String,
                 * age: String,
                 * sex: String,
                 * species: String,
                 * imgPet: Int,
                 * description: String,
                 * personality: String,
                 * history: String
                 */
                var dialogFragment = MascotaDetailDialogFragment().newInstance(
                    mascota.id,
                    mascota.name,
                    mascota.age,
                    mascota.sex,
                    mascota.species,
                    mascota.imgPet,
                    mascota.description,
                    mascota.personality,
                    mascota.vetHistory
                )
                dialogFragment.show(fragmentManager, "Detalle")
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MascotaHolder {
        var layoutInflater = LayoutInflater.from(parent.context)

        return MascotaHolder(layoutInflater.inflate(R.layout.mascota_row, parent, false), fragmentManager)
    }

    override fun onBindViewHolder(holder: MascotaHolder, position: Int) {
        holder.render(mascotasList[position])
    }

    override fun getItemCount(): Int {
        return mascotasList.size
    }
}
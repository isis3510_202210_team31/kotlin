package com.example.mascotas.view.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.example.mascotas.R
import com.example.mascotas.databinding.ActivityRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class RegisterActivity : AppCompatActivity() {

    private lateinit var etFirstName: EditText
    private lateinit var etLastName: EditText
    private lateinit var etCellPhone: EditText
    private lateinit var etEmail: EditText
    private lateinit var etPassword: EditText
    private lateinit var btnRegister: Button
    lateinit var tvRedirectLogin: TextView

    private lateinit var auth: FirebaseAuth

    private lateinit var binding : ActivityRegisterBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*
        etFirstName = findViewById(R.id.firstName)
        etLastName = findViewById(R.id.lastName)
        etCellPhone = findViewById(R.id.cellphone)
        etEmail = findViewById(R.id.email)
        etPassword = findViewById(R.id.password)
        btnRegister = findViewById(R.id.registrar)
        tvRedirectLogin = findViewById(R.id.redirectLogin)*/

        /** NOTA: No hay necesidad de hacer databinding porque no hay informacion reflejada.
         * Cuando requiera mostrar esta informacion la extraere.
         */
        etFirstName = binding.firstName
        etLastName = binding.lastName
        etCellPhone = binding.cellphone
        etEmail = binding.email
        etPassword = binding.password
        btnRegister = binding.registrar
        tvRedirectLogin = binding.redirectLogin

        /**
         * Proceso de registrarse.
         */
        auth = Firebase.auth

        btnRegister.setOnClickListener {
            signUpUser()
        }

        tvRedirectLogin.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            // using finish() to end the activity
            finish()
        }
    }

    /**
     * Funcion para registrar al usuario
     */
    private fun signUpUser() {
        val firstName = etFirstName.text.toString()
        val lastName = etFirstName.text.toString()
        val cellphone = etFirstName.text.toString()
        val email = etEmail.text.toString()
        val pass = etPassword.text.toString()

        // check pass
        if (email.isBlank() || pass.isBlank() || firstName.isBlank() || lastName.isBlank() || cellphone.isBlank()) {
            Toast.makeText(
                this,
                "Hay campos vacios, por favor vuelva a revisar",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        /**
         * Registramos el usuario en Firebase si la info es correcta.
         */
        auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this) {
            if (it.isSuccessful) {
                //TODO Aqui debo guardar toda la informacion ingresada luego en la BD
                Toast.makeText(this, "Se registro correctamente", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "El registro no pudo completarse", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
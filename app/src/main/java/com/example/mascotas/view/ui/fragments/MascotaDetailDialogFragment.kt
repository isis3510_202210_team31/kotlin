package com.example.mascotas.view.ui.fragments

import android.app.Dialog
import android.content.res.Resources
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.mascotas.R
import com.example.mascotas.databinding.FragmentMascotaDetailDialogBinding

class MascotaDetailDialogFragment : DialogFragment() {

    private var id: Long? = null
    private var name: String? = null
    private var age: String? = null
    private var sex: String? = null
    private var species: String? = null
    private var imgPet: Int? = null
    private var description: String? = null
    private var personality: String? = null
    private var vetHistory: String? = null

    fun newInstance(
        id: Long,
        name: String,
        age: String,
        sex: String,
        species: String,
        imgPet: Int,
        description: String,
        personality: String,
        vetHistory: String
    ): MascotaDetailDialogFragment {
        var mascotaDetailDialogFragment = MascotaDetailDialogFragment()

        val args = Bundle()
        args.putLong("id", id)
        args.putString("name", name)
        args.putString("age", age)
        args.putString("sex", sex)
        args.putString("species", species)
        args.putInt("imgPet", imgPet)
        args.putString("description", description)
        args.putString("personality", personality)
        args.putString("vetHistory", vetHistory)

        mascotaDetailDialogFragment.arguments = args

        return  mascotaDetailDialogFragment
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            id = it.getLong("id")
            name = it.getString("name")
            age = it.getString("age")
            sex = it.getString("sex")
            species = it.getString("species")
            imgPet = it.getInt("imgPet")
            description = it.getString("description")
            personality = it.getString("personality")
            vetHistory = it.getString("vetHistory")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var binding = FragmentMascotaDetailDialogBinding.bind(view)

        //TODO Bindiar o asignarle una imagen al detail
        //imgPet = binding.imgMascota.setImageResource()

        //TODO Por alguna razon no aparece el texto descriptivo asi que se agrego aqui. Solucionar!!!
        binding.tvNombre.text = name
        binding.tvEspecie.text = species
        binding.tvDescEdad.text = "Posible edad: " + age
        binding.tvDescSexo.text = "Sexo: " +sex
        binding.tvDescPersonalidad.text = "Personalidad: " + personality
        binding.tvDescDescripcion.text = "Descripcion: " + description
        binding.tvDescHistorial.text = "Historial veterinario: " + vetHistory

        //TODO OnClickListener para chat o lo que sea
    }


    /**
     * https://stackoverflow.com/questions/12478520/how-to-set-dialogfragments-width-and-height
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //setFullScreen()
        setSize(95,85)
    }

    fun DialogFragment.setFullScreen() {
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }

    fun DialogFragment.setSize(percentageWidth: Int , percentageHeight: Int) {
        val percentWidth = percentageWidth.toFloat() / 100
        val percentHeight = percentageHeight.toFloat() / 100
        val dm = Resources.getSystem().displayMetrics
        val rect = dm.run { Rect(0, 0, widthPixels, heightPixels) }
        val _percentWidth = rect.width() * percentWidth
        val _percentHeight = rect.height() *percentHeight
        //dialog?.window?.setLayout(percentWidth.toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog?.window?.setLayout(_percentWidth.toInt(), _percentHeight.toInt())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_mascota_detail_dialog, container, false)
    }
}
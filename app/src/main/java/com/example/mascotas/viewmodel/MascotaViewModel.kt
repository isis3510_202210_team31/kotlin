package com.example.mascotas.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mascotas.model.MascotaModel
import com.example.mascotas.model.MascotasProvider

class MascotaViewModel : ViewModel() {

    var mascotaProvider = MascotasProvider
    var mascotasModel = MutableLiveData<List<MascotaModel>>()
    var mascotaModel = MutableLiveData<MascotaModel>()

    fun getMascotas() {
        var currentMascotas = mascotaProvider.getMascotas(mascotasModel)
    }

}
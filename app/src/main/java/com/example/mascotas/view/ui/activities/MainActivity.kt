package com.example.mascotas.view.ui.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.RelativeLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI

import com.example.mascotas.R
import com.example.mascotas.databinding.ActivityMainBinding
import com.example.mascotas.view.ui.fragments.ChatFragment
import com.example.mascotas.view.ui.fragments.MascotasListFragment
import com.example.mascotas.view.ui.fragments.SearchFragment
import com.example.mascotas.view.ui.fragments.UsuarioFragment


class MainActivity : AppCompatActivity() {

    private lateinit var btnLogOut: Button
    private lateinit var relativeLayout: RelativeLayout

    private val mascotasListFragment = MascotasListFragment()
    private val chatFragment = ChatFragment()
    private val searchFragment = SearchFragment()
    private val usuarioFragment = UsuarioFragment()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        )

        relativeLayout = binding.relative

        replaceFragment(mascotasListFragment)

        var bnvBar = binding.bnvMenu

        bnvBar.setOnItemSelectedListener {
            when(it.itemId){
                R.id.navHome -> replaceFragment(mascotasListFragment)
                R.id.navSearch -> replaceFragment(searchFragment)
                R.id.navChat -> replaceFragment(chatFragment)
                R.id.navUser -> replaceFragment(usuarioFragment)
            }
            true
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        if (fragment != null) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragContent, fragment)
            transaction.commit()

        }
    }

}
package com.example.mascotas.view.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mascotas.R
import com.example.mascotas.view.adapter.MascotaListAdapter
import com.example.mascotas.viewmodel.MascotaViewModel


class MascotasListFragment : Fragment() {

    private val viewModel: MascotaViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //TODO Importante
        viewModel.getMascotas()

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mascotas_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //super.onViewCreated(view, savedInstanceState)
        var rvMascotas = view.findViewById<RecyclerView>(R.id.rvMascotas)
        rvMascotas.layoutManager = LinearLayoutManager(requireActivity())

        viewModel.mascotasModel.observe(viewLifecycleOwner){ mascotas ->
            val adapter = MascotaListAdapter(mascotas, childFragmentManager)
            rvMascotas.adapter = adapter
        }
    }
}
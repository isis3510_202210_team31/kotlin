package com.example.mascotas.view.ui.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.findFragment
import com.example.mascotas.R
import com.example.mascotas.view.ui.activities.LoginActivity
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class UsuarioFragment : Fragment() {

    private lateinit var btnLogOut: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        /**
         * Aqui hay logica para desconectarse.
         */
        /*btnLogOut = findViewById(R.id.logout)

        btnLogOut.setOnClickListener() {
            Firebase.auth.signOut()

            //Una vez cerramos sesion, vamos al login
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            // using finish() to end the activity
            finish()
        }*/

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_usuario, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnLogOut = view.findViewById(R.id.logout)

        btnLogOut.setOnClickListener() {
            Firebase.auth.signOut()

            val intent: Intent = Intent(activity?.applicationContext, LoginActivity::class.java)
            activity?.startActivity(intent)
            onDestroy()
        }
    }
}
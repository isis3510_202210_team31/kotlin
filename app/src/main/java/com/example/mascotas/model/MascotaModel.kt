package com.example.mascotas.model

import androidx.annotation.DrawableRes


data class MascotaModel(
    val id: Long,
    val name: String,
    val age: String,
    val sex: String,
    val species: String,

    //TODO Esto se puede hacer string y bla bla
    @DrawableRes val imgPet: Int,
    val description: String,
    val personality: String,
    /*TODO Volver tipo list*/
    val vetHistory: String
)

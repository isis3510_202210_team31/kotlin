package com.example.mascotas.model

import androidx.lifecycle.MutableLiveData
import com.example.mascotas.R

/**
 * TODO Base de datos de paja. Solo testeo.
 */
object MascotasProvider {


    fun getMascota(id: Int): MascotaModel {
        return mascotas.get(id)
    }

    fun getMascotas(mutableLiveData: MutableLiveData<List<MascotaModel>>){
        mutableLiveData.value = mascotas
    }

    val mascotas = listOf<MascotaModel>(
        MascotaModel(
            1,
            "Beluga",
            "12",
            "Desconocido",
            "Gato",
            R.drawable.beluga_example,
            "historialVet",
            "Es tierno",
            "Historial"
        ),
        MascotaModel(
            2,
            "Narla",
            "12",
            "Hembra",
            "Gato",
            R.drawable.beluga_example,
            "historialVet",
            "Es tierno",
            "Historial"
        ),
        MascotaModel(
            3,
            "Matias",
            "12",
            "Macho",
            "Gato",
            R.drawable.beluga_example,
            "historialVet",
            "Es tierno",
            "Historial"
        ),
        MascotaModel(
            4,
            "Tayson",
            "12",
            "Macho",
            "Perro",
            R.drawable.beluga_example,
            "historialVet",
            "Es tierno",
            "Historial"
        ),
        MascotaModel(
            5,
            "Simona",
            "12",
            "Hembra",
            "Gato",
            R.drawable.beluga_example,
            "historialVet",
            "Es tierno",
            "Historial"
        )
    )
}